import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    username: string;
    password: string;
    error: string;

    constructor(private userService: UserService, private router: Router) { }

    ngOnInit() {
    }

    login(): void {

      this.userService.login(this.username, this.password).subscribe(
          loginStatus => {

              // Success - redirect

              this.error = null;
              this.router.navigate(['']);

          },
          error => {

              // Something went wrong, tell the user

              this.error = 'You must supply a valid username and password.';

          }
      );

    }

}
