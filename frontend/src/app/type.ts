/**
 * Represents a possible cart type.
 */

export class Type {

    type: string;
    colour: string;

}
