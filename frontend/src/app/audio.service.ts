import { Injectable } from '@angular/core';
import { reject, Promise } from 'q';
import { isNull, isNullOrUndefined } from 'util';
import { Settings } from './settings';
import { Cart } from './cart';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  private static positionTitleField = 12;
  private static positionArtistField = 76;
  private static lengthTitleField = 64;
  private static lengthArtistField = 64;
  private static positionMarker = 692;
  private static lengthMarker = 8;
  private static countMarker = 8;
  private static positionSampleRate = 12;

  constructor() { }

  /**
   * Loads audio from a supplied file.
   * @param file The file to load.
   * @param context The audio context to load into.
   */

  public loadFromFile(file: File, context: AudioContext): Promise<AudioBuffer> {

    return Promise((resolve) => {

      const fileReader = new FileReader();

      fileReader.onload = function(progressEvent) {
        context.decodeAudioData(<ArrayBuffer> this.result)
          .then(audio => resolve(audio))
          .catch((error) => reject(`Failed to load audio file ${file.name}.`));
      };

      fileReader.readAsArrayBuffer(file);

    });

  }

  /**
   * Loads audio from a supplied blob.
   * @param blob The file blob to load.
   * @param context The audio context to load into.
   */

  public loadFromBlob(blob: Blob, context: AudioContext): Promise<AudioBuffer> {

    return Promise((resolve) => {

      const fileReader = new FileReader();

      fileReader.onload = function(progressEvent) {
        context.decodeAudioData(<ArrayBuffer> this.result)
          .then(audio => resolve(audio))
          .catch((error) => reject('Failed to load audio from supplied blob.'))
      };

      fileReader.readAsArrayBuffer(blob);

    });

  }

  /**
   * Normalises audio.
   * @param audio The audio to be normalised.
   * @param settings The settings we'll be using.
   */

  public normalise(audio: AudioBuffer, settings: Settings): Promise<AudioBuffer> {

    return Promise((resolve) => {

      // Sanity check

      if (isNull(audio)) {
        reject('No valid audio supplied.');
      }

      // Look for the max value in each channel

      let maxValue = 0.0;

      for (let i = 0; i < audio.numberOfChannels; i++) {

        // Look for the max value

        const currentChannel = audio.getChannelData(i);
        for (const j in currentChannel) {
          if (Math.abs(currentChannel[j]) > maxValue) {
            maxValue = Math.abs(currentChannel[j]);
          }
        }

      }

      // Perform the normalisation

      const scaleFactor = settings.normalise.linear / maxValue;

      for (let i = 0; i < audio.numberOfChannels; i++) {

        // Rescale the audio

        const currentChannel = audio.getChannelData(i);
        for(let j = 0; j < currentChannel.length; j++) {
          currentChannel[j] *= scaleFactor;
        }

        // Save it back to the audio buffer

        audio.copyToChannel(currentChannel, i);

      }

      // Pass the job back

      resolve(audio);

    });

  }

  public readMetadata(file: File, cart: Cart): Promise<Cart> {

    return Promise((resolve) => {

      // Sanity check

      if (isNull(file)) {
        reject('You must supply a valid audio file for us to read metadata from.');
        return;
      }

      // See if we need a new cart object

      if (isNull(cart)) {
        cart = new Cart();
      }

      // Try to extract the label from the filename

      let strippedName = file.name.replace(/\.[^/.]+$/, '');
      strippedName = strippedName.replace(/[^0-9^A-Z^a-z]+/g, '');
      if (strippedName.length > 0 && (isNullOrUndefined(cart.label) || cart.label.length === 0)) {
        cart.label = strippedName;
      }

      // Read the file into memory

      const fileReader = new FileReader();
      fileReader.onload = function(progressEvent) {

        // Check if it's a WAVE file

        const dataView = new DataView(<ArrayBuffer>this.result);
        const buffer = new Uint8Array(<ArrayBuffer>this.result);
        if (buffer.length < 4) {
          reject(`${file.name} is not a valid WAVE file.`);
          return;
        }

        const header = String.fromCharCode.apply(null, buffer.subarray(0, 4));
        if (header !== 'RIFF') {
          reject(`${file.name} is not a valid WAVE file.`);
          return;
        }

        // We're all good, scan through the chunks looking for the CART

        let sampleRate = 44100;
        let offset = 12;
        while (offset < buffer.length) {

          // Read the type of chunk we've got

          const type = String.fromCharCode.apply(null, buffer.subarray(offset, offset + 4));
          const length = dataView.getUint32(offset + 4, true);

          if (type === 'cart') {

            // Read in the artist and title

            cart.title = String.fromCharCode.apply(
              null,
              buffer.subarray(
                offset + AudioService.positionTitleField,
                offset + AudioService.positionTitleField + AudioService.lengthTitleField
                )
            );

            cart.display_artist = String.fromCharCode.apply(
              null,
              buffer.subarray(
                offset + AudioService.positionArtistField,
                offset + AudioService.positionArtistField + AudioService.lengthArtistField
                )
            );

            // Cycle through the markers

            for (let i = 0; i < AudioService.countMarker; i++) {

              const currentMarkerPosition = offset + AudioService.positionMarker + (AudioService.lengthMarker * i);
              const markerType = String.fromCharCode.apply(null, buffer.subarray(currentMarkerPosition, currentMarkerPosition + 4));
              const markerValue = Math.round(dataView.getUint32(currentMarkerPosition + 4, true) / sampleRate * 10) / 10;

              // Update our markers

              if (markerType === 'AUDs') {
                cart.marker_audio_start = markerValue;
              } else if (markerType === 'AUDe') {
                cart.marker_audio_end = markerValue;
              } else if (markerType === 'INT ' || markerType === 'INT1' || markerType === 'INTs') {
                cart.marker_intro_start = markerValue;
              } else if (markerType === 'INT2' || markerType === 'INTe') {
                cart.marker_intro_end = markerValue;
              } else if (markerType === 'SEG ' || markerType === 'SEG1' || markerType === 'OUT ') {
                cart.marker_segue = markerValue;
              }

            }

            // Cleanup

            cart.title = cart.title.trim();
            cart.display_artist = cart.display_artist.trim();

            cart.artists = [cart.display_artist];

            resolve(cart);
            return;

          } else if (type === 'fmt ') {
            sampleRate = dataView.getUint32(offset + AudioService.positionSampleRate, true);
          }

          // Loop round

          offset += length + 8;

        }

        // If we got nothing, just return the cart

        resolve(cart);

      };

      fileReader.readAsArrayBuffer(file);

    });

  }

  /**
   * Generates a wave file.
   * @param cart The cart we'll pull metadata from.
   * @param AudioBuffer The audio we wish to load into the file.
   */

  public generateWaveFile(cart: Cart, audio: AudioBuffer): Promise<Blob> {

    // Sanity check

    if (!cart || !audio) {
      return null;
    }

    return Promise((resolve) => {

      // Generate the chunks

      const formatChunk = this.generateFormatChunk(audio);
      const dataChunk = this.generateDataChunk(audio);
      const cartChunk = this.generateCartChunk(cart, audio);
      const headerChunk = this.generateHeaderChunk([formatChunk, cartChunk, dataChunk]);

      // Splat them together in a blob

      resolve(new Blob([headerChunk, formatChunk, cartChunk, dataChunk]));


    });

  }

  /**
   * Generates the RIFF header chunk.
   * @param chunks The chunks we'll be writing the header for.
   */

  private generateHeaderChunk(chunks: ArrayBuffer[]) : ArrayBuffer {

    const headerLength = 12;
    const chunk = new ArrayBuffer(headerLength);

    // Calculate our inner length

    let length = 0;
    for (const currentChunk of chunks) {
      length += currentChunk.byteLength;
    }

    this.insertChunkHeader(chunk, 'RIFF', length + 4);

    // Append the WAVE format header to the chunk

    const waveHeader = new Uint8Array(chunk, 8, 4);
    this.insertStringToByteArray('WAVE', waveHeader);

    return chunk;

  }

  /**
   * Generates the cart chunk for a wave file.
   * @param cart The cart we're generating the chunk for.
   * @param audio The audio we're working with.
   */

  private generateCartChunk(cart: Cart, audio: AudioBuffer): ArrayBuffer {

    const chunkSize = 1960;
    const headerSize = 8;
    const chunk = new ArrayBuffer(chunkSize + headerSize);

    // Splat in the header

    this.insertChunkHeader(chunk, 'cart', chunkSize);

    // Fill out the info fields

    const version = new Uint8Array(chunk, 8, 4);
    const title = new Uint8Array(chunk, 12, 64);
    const artist = new Uint8Array(chunk, 76, 64);
    const cutNumber = new Uint8Array(chunk, 140, 64);
    const appName = new Uint8Array(chunk, 496, 64);
    const appVersion = new Uint8Array(chunk, 560, 64);
    const referenceLevel = new Int32Array(chunk, 624, 1);

    this.insertStringToByteArray('0101', version);
    this.insertStringToByteArray(cart.title, title);
    this.insertStringToByteArray(cart.display_artist, artist);
    this.insertStringToByteArray(cart.label, cutNumber);
    this.insertStringToByteArray('AllDay DJ', appName);
    this.insertStringToByteArray('Angular Web App', appVersion);
    referenceLevel[0] = 32768;

    // Markers

    this.insertMarker(chunk, 'AUDs', cart.marker_audio_start, 0, audio.sampleRate);
    this.insertMarker(chunk, 'AUDe', cart.marker_audio_end, 1, audio.sampleRate);
    this.insertMarker(chunk, 'INTs', cart.marker_intro_start, 2, audio.sampleRate);
    this.insertMarker(chunk, 'INTe', cart.marker_intro_end, 3, audio.sampleRate);
    this.insertMarker(chunk, 'SEG ', cart.marker_segue, 4, audio.sampleRate);

    return chunk;

  }

  /**
   * Inserts a marker into the cart chunk.
   * @param buffer The buffer holding the cart chunk.
   * @param name The name of the marker.
   * @param position The position (in seconds) of the marker.
   * @param markerNumber The number marker in the field.
   * @param sampleRate The sample rate of the audio.
   */

  private insertMarker(buffer: ArrayBuffer, name: string, position: number, markerNumber: number, sampleRate: number) {

    const markerStart = AudioService.positionMarker + markerNumber * 8;
    const nameField = new Uint8Array(buffer, markerStart, 4);
    const positonField = new Uint32Array(buffer, markerStart + 4, 1);

    this.insertStringToByteArray(name, nameField);
    positonField[0] = position * sampleRate;

  }

  /**
   * Inserts a text string into a byte array.
   * @param text The text to insert into the array
   * @param array The array we want the text in.
   * @param maxLength The maximum allowed length of the field.
   */

  private insertStringToByteArray(text: string, array: Uint8Array, maxLength = -1) {

    // Sanity check

    if (!text) {
      return;
    }

    // Perform the transformation

    const allowedLength = maxLength > 0 ? Math.min(maxLength, array.length, text.length) : Math.min(text.length, array.length);
    for (let i = 0; i < allowedLength; i++) {
      array[i] = text.charCodeAt(i);
    }

  }

  /**
   * Generates the data chunk for a wave file.
   * @param audio The audio we'll be generating the chunk from.
   */

  private generateDataChunk(audio: AudioBuffer): ArrayBuffer {

    const bitsPerSample = 16;
    const headerLength = 8;
    const maxInt16 = 32768;

    // Calculate our chunk size and generate a buffer

    const chunkSize = audio.numberOfChannels * audio.length * bitsPerSample / 8;
    const chunk = new ArrayBuffer(chunkSize + headerLength);

    // Splat in the header

    this.insertChunkHeader(chunk, 'data', chunkSize);

    // Interlace the audio data

    const channels: Float32Array[] = [];
    for (let i = 0; i < audio.numberOfChannels; i++) {
      channels.push(audio.getChannelData(i));
    }

    const dataBuffer = new Int16Array(chunk, headerLength, chunkSize / bitsPerSample * 8);
    for (let i = 0; i < audio.length; i++) {
      for (let j = 0; j < channels.length; j++) {
        dataBuffer[i * channels.length + j] = channels[j][i] * maxInt16;
      }
    }

    return chunk;

  }

  /**
   * Inserts the header text and length into the chunk.
   * @param chunk The chunk to insert the header in to.
   * @param headerText The text to insert.
   */

  private insertChunkHeader(chunk: ArrayBuffer, headerText: string, length: number) {

    const header = new Uint8Array(chunk, 0, 4);
    const chunkSize = new Uint32Array(chunk, 4, 1);

    this.insertStringToByteArray(headerText, header);
    chunkSize[0] = length;

  }

  /**
   * Generates the format chunk for a wave file.
   * @param audio The audio we're generating the chunk from.
   */

  private generateFormatChunk(audio: AudioBuffer): ArrayBuffer {

    const chunk = new ArrayBuffer(24);
    const bitsPerSample = 16;
    const chunkSize = 16;

    // Header

    this.insertChunkHeader(chunk, 'fmt ', chunkSize);

    // We default to 16 bit PCM audio with the correct number of channels

    const pcm = new Uint16Array(chunk, 8, 1);
    const channels = new Uint16Array(chunk, 10, 1);
    const sampleRate = new Uint32Array(chunk, 12, 1);
    const byteRate = new Uint32Array(chunk, 16, 1);
    const blockAlign = new Uint16Array(chunk, 20, 1);
    const bitsPerSampleArray = new Uint16Array(chunk, 22, 1);

    pcm[0] = 1;
    channels[0] = audio.numberOfChannels;
    sampleRate[0] = audio.sampleRate;
    byteRate[0] = audio.sampleRate * audio.numberOfChannels * bitsPerSample / 8;
    blockAlign[0] = audio.numberOfChannels * bitsPerSample / 8;
    bitsPerSampleArray[0] = bitsPerSample;

    return chunk;

  }

}
