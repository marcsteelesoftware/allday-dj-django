import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
    selector: '[appCartLabel]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: CartLabelDirective,
            multi: true
        }
    ]
})

/**
 * Provides validation for cart labels
 */

export class CartLabelDirective implements Validator {

    validate(control: AbstractControl) {

        const suppliedLabel = control.value;
        const regex = new RegExp('^[0-9a-zA-Z]+$');
        return regex.test(suppliedLabel) ? null : {validLabel: false};

    }

}