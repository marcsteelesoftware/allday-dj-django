export class Artist {

    id: number;
    name: string;

    constructor(name: string) {
        this.name = name;
    }

}