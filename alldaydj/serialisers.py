'''
Application Specific Serialisers

@author: Marc Steele
'''

from rest_framework import serializers
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text
from alldaydj.models import Cart, Category, Tag, Type, Artist

class UserSerialiser(serializers.ModelSerializer):
    """Serialises user objects"""
    class Meta:
        model = User
        fields = ('username', )

class CategorySerialiser(serializers.ModelSerializer):
    """Serialiser for scheduler categories"""
    class Meta:
        model = Category
        fields = ('category', )

class TagSerialiser(serializers.ModelSerializer):
    """Serialiser for scheduler tags"""
    class Meta:
        model = Tag
        fields = ('tag', )

class TypeSerialiser(serializers.ModelSerializer):
    """Serialiser for cart types"""
    class Meta:
        model = Type
        fields = ('type', 'now_playing_enabled', 'colour')

class CreatableSlugRelatedField(serializers.SlugRelatedField):
    """Provides an override for any slug related field field to allow creating on the fly"""
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')

class CartSerialiser(serializers.ModelSerializer):
    """Serisliser for carts"""
    artists = CreatableSlugRelatedField(
        many=True,
        read_only=False,
        queryset=Artist.objects.all(),
        slug_field='name')

    tags = serializers.SlugRelatedField(
        many=True,
        read_only=False,
        queryset=Tag.objects.all(),
        slug_field='tag')

    categories = serializers.SlugRelatedField(
        many=True,
        read_only=False,
        queryset=Category.objects.all(),
        slug_field='category')

    type = serializers.SlugRelatedField(
        many=False,
        read_only=False,
        queryset=Type.objects.all(),
        slug_field='type')

    class Meta:
        model = Cart
        exclude = ('id', )
