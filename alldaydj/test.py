'''
AllDay DJ Unit Tests
'''

import json
from rest_framework.test import APITestCase
from django.urls import reverse
from django.contrib.auth.models import User

class TestIndex(APITestCase):
    '''
    Tests we can access the index
    '''

    def test_index_unauthenticated(self):
        '''
        Checks we get a redirect if we're not authenticated
        '''

        # Arrange

        url = '/'

        # Act

        response = self.client.get(url)

        # Assert

        self.assertEqual(200, response.status_code)

    test_login_params = [
        ("super_duper_user", "user1@example.com", "s0m3P@w0rd"),
        ("AnotherUser", "user2@example.net", "withsomecredz!")
    ]

    def test_index_authenticated(self):
        '''
        Checks we can log in successfully
        '''

        # Run this test multiple times

        for (username, email, password) in self.test_login_params:
            with self.subTest(username=username, email=email, password=password):

                # Arrange

                User.objects.create_user(username, email=email, password=password)
                self.client.login(username=username, password=password)
                url = '/'

                # Act

                response = self.client.get(url)

                # Assert

                self.assertEqual(200, response.status_code)

class TestLoginScreen(APITestCase):
    '''
    Tests we can log in and out as required.
    '''

    test_login_params = [
        ("super_duper_user", "user1@example.com", "s0m3P@w0rd"),
        ("AnotherUser", "user2@example.net", "withsomecredz!")
    ]

    def test_login_valid(self):
        '''
        Checks we can log in successfully
        '''

        # Run this test multiple times

        for (username, email, password) in self.test_login_params:
            with self.subTest(username=username, email=email, password=password):

                # Arrange

                User.objects.create_user(username, email=email, password=password)
                url = reverse('apilogin')
                post_parts = {
                    'username': username,
                    'password': password
                }

                # Act

                response = self.client.post(url, post_parts)
                json_response = json.loads(response.content)

                # Assert

                self.assertEqual(200, response.status_code)
                self.assertEqual(username, json_response['username'])

    def test_login_invalid(self):
        '''
        Checks we can't login without a valid account
        '''

        # Run this test multiple times

        for (username, email, password) in self.test_login_params:
            with self.subTest(username=username, email=email, password=password):

                # Arrange

                url = reverse('apilogin')
                post_parts = {
                    'username': username,
                    'password': password
                }

                # Act

                response = self.client.post(url, post_parts)

                # Assert

                self.assertEqual(401, response.status_code)

class TestApplicationSettings(APITestCase):
    '''
    Tests the application settings page
    '''

    test_login_params = [
        ("super_duper_user", "user1@example.com", "s0m3P@w0rd"),
        ("AnotherUser", "user2@example.net", "withsomecredz!")
    ]

    def test_settings_authenticated(self):
        '''
        Checks we get the default settings back when authenticated
        '''

        # Run this test multiple times

        for (username, email, password) in self.test_login_params:
            with self.subTest(username=username, email=email, password=password):

                # Arrange

                User.objects.create_user(username, email=email, password=password)
                self.client.login(username=username, password=password)
                url = reverse('settings')

                # Act

                response = self.client.get(url)
                json_response = json.loads(response.content)

                # Assert

                self.assertEqual(200, response.status_code)
                self.assertEqual(-3, json_response['normalise']['dBFS'])
                self.assertEqual(0.7079457843841379, json_response['normalise']['linear'])
                self.assertEqual(10, json_response['years_future'])

    def test_settings_unauthenticated(self):
        '''
        Checks we don't get the settings back when unauthenticated.
        '''

        # Arrange

        url = reverse('settings')

        # Act

        response = self.client.get(url)

        # Assert

        self.assertEqual(403, response.status_code)
