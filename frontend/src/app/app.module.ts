import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { CartListComponent } from './cart-list/cart-list.component';
import { CartEditorComponent } from './cart-editor/cart-editor.component';
import { CartLabelDirective } from './cart-label.directive';
import { CartPreviewComponent } from './cart-preview/cart-preview.component';
import { UnauthenticatedInterceptor } from './interceptors/unauthenticated.interceptor';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'cart',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'cart',
        component: CartListComponent
    },
    {
        path: 'cart/new',
        component: CartEditorComponent
    },
    {
        path: 'cart/edit/:label',
        component: CartEditorComponent
    }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CartListComponent,
    CartEditorComponent,
    CartLabelDirective,
    CartPreviewComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    NgbModule,
  ],
  exports: [
      RouterModule
  ],
  providers: [
      {
          provide: APP_BASE_HREF,
          useValue: '/'
      },
      {
          provide: HTTP_INTERCEPTORS,
          useClass: UnauthenticatedInterceptor,
          multi: true
      },
      CookieService,
      NgbModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
