import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Provides a component base class with utility methods built-in.
 */

export class ComponentBase {

    protected errorMessage: string;

    constructor(
        protected modalService: NgbModal
    ) {}

    /**
     * Handles errors with loading something from the server.
    * @param error The error to be handled.
    * @param errorModal The screen to show the error in.
    */

    protected handleHttpLoadError(error: HttpErrorResponse, errorModal: any) {

        if (error.error instanceof ErrorEvent) {
            this.displayError(true, `A local error occurred: ${error.error.message}`, errorModal);
        } else {
            this.displayError(true, `A server error occurred. Server status code ${error.status} with message ${error.error.message}`, errorModal);
        }

    }

  

  /**
   * Displays an error message.
   * @param error Whether to display the message or not.
   * @param errorModal The modal screen to display the error in.
   * @param message The message.
   */

  protected displayError(error: boolean, errorModal: any, message: string) {

    this.errorMessage = message;
    this.modalService.dismissAll();

    if (error) {
      this.modalService.open(errorModal);
    }

  }

}