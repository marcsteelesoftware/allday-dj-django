import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  user: User;
  navbarCollapsed = true;

  constructor(private userService: UserService, private router: Router) {

      // Sign up to routing events

      router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
            this.getUser();
        }
      });

   }

  ngOnInit() {
      this.getUser();
  }

  /**
   * Updates the currently logged in user.
   */

  getUser(): void {
      this.userService.getUser().subscribe(user => this.user = user);
  }

  /**
   * Logs the user out.
   */

  logout(): void {
    this.userService.logout().subscribe(
      response => {

          // All good, trigger an update

          this.getUser();

      },
      error => {

          // No good, trigger an update anyway

          this.getUser();

      }
    );
  }

}
