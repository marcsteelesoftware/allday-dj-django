export class Settings {
    normalise: {
        dbFS: -3,
        linear: 0.7079457843841379,
    };
    years_future = 10;
}
