'''
AllDay DJ Data Models

@author: Marc Steele
'''

from datetime import datetime
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_delete
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.core.files.storage import default_storage
from colorful.fields import RGBColorField

# Utility methods

def station_thumbnail_path(instance, filename):
    """Generates the thumbnail path for a station"""

    if instance:
        return f'stations/thumbnails/{instance.id}_{filename}'

    return None

# Validators

ALPHANUMERIC_VALIDATOR = RegexValidator(r'^[0-9a-zA-Z]+$')

# Models

class Station(models.Model):
    """Represents a possible radio station"""
    name = models.TextField(unique=True)
    logo = models.ImageField(upload_to=station_thumbnail_path, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Service(models.Model):
    """A service associated with a station"""
    name = models.TextField(unique=True)
    station = models.ForeignKey(Station, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Artist(models.Model):
    """An artist on a cart"""
    name = models.TextField(unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Category(models.Model):
    """A category for scheduling carts"""
    category = models.TextField(unique=True)

    def __str__(self):
        return self.category

    class Meta:
        ordering = ['category']

class Type(models.Model):
    """A possible cart type"""
    type = models.TextField(unique=True)
    now_playing_enabled = models.BooleanField()
    colour = RGBColorField()

    def __str__(self):
        return self.type

    class Meta:
        ordering = ['type']

class Tag(models.Model):
    """A tag that will be used in the scheduling process"""
    tag = models.TextField(unique=True)

    def __str__(self):
        return self.tag

    class Meta:
        ordering = ['tag']

class Cart(models.Model):
    """An audio cart"""
    label = models.TextField(unique=True, validators=[ALPHANUMERIC_VALIDATOR])
    display_artist = models.TextField()
    artists = models.ManyToManyField(Artist, blank=True)
    title = models.TextField()
    year = models.IntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(datetime.now().year + settings.ADDJ_YEARS_FUTURE)
        ]
    )
    isrc = models.TextField(blank=True)
    record_label = models.TextField(blank=True)
    composer_arranger = models.TextField(blank=True)
    publisher = models.TextField(blank=True)
    promoter = models.TextField(blank=True)
    categories = models.ManyToManyField(Category, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    sweeper = models.BooleanField(default=False)
    type = models.ForeignKey(Type, on_delete=models.SET_NULL, blank=True, null=True)
    marker_audio_start = models.DecimalField(
        default=0.0,
        decimal_places=1,
        max_digits=settings.ADDJ_MARKER_MAX_DIGITS
    )
    marker_audio_end = models.DecimalField(
        default=0.0,
        decimal_places=1,
        max_digits=settings.ADDJ_MARKER_MAX_DIGITS
    )
    marker_intro_start = models.DecimalField(
        default=0.0,
        decimal_places=1,
        max_digits=settings.ADDJ_MARKER_MAX_DIGITS
    )
    marker_intro_end = models.DecimalField(
        default=0.0,
        decimal_places=1,
        max_digits=settings.ADDJ_MARKER_MAX_DIGITS
    )
    marker_segue = models.DecimalField(
        default=0.0,
        decimal_places=1,
        max_digits=settings.ADDJ_MARKER_MAX_DIGITS
    )

    def __str__(self):
        return f'({self.label}) {self.display_artist} - {self.title}'

@receiver(post_delete, sender=Cart)
def cart_delete_handler(sender, **kwargs): # pylint: disable=unused-argument
    """
    Handles carts being deleted and cleans up
    anything in the filestore.
    """

    filenames = [
        f'{sender.label}.wav',
        f'{sender.label}.ogg'
    ]

    for filename in filenames:
        if default_storage.exists(filename):
            default_storage.delete(filename)
