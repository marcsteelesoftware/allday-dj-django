'''
Admin Configuration

@author: Marc Steele
'''

from django.contrib import admin
from alldaydj.models import Service, Station, Category, Tag, Type, Cart, Artist

admin.site.register(Service)
admin.site.register(Station)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Type)

# TEMP

admin.site.register(Cart)
admin.site.register(Artist)
