'''
The views element of MVC.

@author: Marc Steele
'''

import math
import wave
import av
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseBadRequest, Http404, FileResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.conf import settings as app_settings
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.static import serve
from django.core.files.storage import default_storage
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from alldaydj.serialisers import UserSerialiser, TagSerialiser, \
    CategorySerialiser, TypeSerialiser, CartSerialiser
from alldaydj.models import Tag, Category, Type, Cart

@ensure_csrf_cookie
def index(request, given_path):
    """Angular driven homepage"""
    context = {}
    return serve(request, '/ang/index.html', document_root=app_settings.STATIC_ROOT)

# REST API

@api_view(http_method_names=['POST'])
@permission_classes([AllowAny])
def api_login(request):
    '''
    Attempts to log the user in
    '''

    # Sanity checks

    if not ('username' in request.data and 'password' in request.data):
        return HttpResponseBadRequest()

    # Attempt the authentication

    username = request.data['username']
    password = request.data['password']
    user = authenticate(username=username, password=password)

    if user is not None:
        login(request, user)
        serialiaser = UserSerialiser(user)
        return JsonResponse(serialiaser.data)
    else:
        return JsonResponse({'login': 'failure'}, status=401)

@api_view(http_method_names=['GET'])
def api_logout(request):
    logout(request)

@api_view(http_method_names=['GET'])
def current_user(request):
    """Obtains the current user"""
    serialiser = UserSerialiser(request.user)
    return JsonResponse(serialiser.data)

@api_view(http_method_names=['GET'])
def settings(request):
    """Obtains the current application settings"""
    settings_container = {}
    settings_container['normalise'] = {
        'dBFS': app_settings.ADDJ_NORMALISE_LEVEL,
        'linear': math.pow(10, app_settings.ADDJ_NORMALISE_LEVEL/20)
        }
    settings_container['years_future'] = app_settings.ADDJ_YEARS_FUTURE

    return JsonResponse(settings_container)

@api_view(http_method_names=['GET', 'POST', 'PUT'])
def wave_file(request, label):
    """WAVE file upload and download"""

    # Make sure we've got a matching cart

    get_object_or_404(Cart, label=label)

    # Setup

    filename = f'{label}.wav'

    # Figure out if we're writing or reading

    if request.method == 'POST' or request.method == 'PUT':

        if request.FILES:

            # We only accept the first supplied file

            uploaded_file_field = list(request.FILES)[0]
            uploaded_file = request.FILES[uploaded_file_field]

            # Sanity check it (make sure we see a WAVE header)

            try:

                wave_file_content = wave.open(uploaded_file, 'r')
                wave_file_content.close()

            except: # pylint: disable=bare-except
                return HttpResponseBadRequest('The supplied WAVE file was not valid!')

            # Delete any existing files

            if default_storage.exists(filename):
                default_storage.delete(filename)

            # Write it to disk

            default_storage.save(filename, uploaded_file)
            return JsonResponse({'upload': 'OK'})

        else:
            return HttpResponseBadRequest('You must supply a file to upload!')

    else:

        # Try to read the file from disk

        if default_storage.exists(filename):
            return FileResponse(
                default_storage.open(filename, 'rb'),
                content_type='audio/wav')
        else:
            raise Http404()

@api_view(http_method_names=['GET'])
def compressed_file(request, label):
    """Compressed (OGG) audio ownload"""

    # Make sure we've got a matching cart

    get_object_or_404(Cart, label=label)

    # Setup

    wave_filename = f'{label}.wav'
    compressed_filename = f'{label}.ogg'

    # Check if we've already got the compressed file

    if (
            default_storage.exists(compressed_filename)
            and default_storage.get_created_time(compressed_filename)
            > default_storage.get_created_time(wave_filename)
    ):
        return FileResponse(
            default_storage.open(compressed_filename, 'rb'),
            content_type='audio/ogg'
        )

    if default_storage.exists(wave_filename):

        # Compress the file, save to storage and punt back to the user

        wave_file_content = av.open(default_storage.open(wave_filename))
        compressed = av.open(default_storage.open(compressed_filename, 'wb'))
        compressed_stream = compressed.add_stream('libvorbis')

        for frame in wave_file_content.decode(audio=0):
            for part in compressed_stream.encode(frame):
                compressed.mux(part)

        compressed.close()
        return FileResponse(
            default_storage.open(
                compressed_filename,
                'rb'
            ),
            content_type='audio/ogg'
        )

    else:
        raise Http404()

# Class Based Views

class TagViewSet(viewsets.ModelViewSet):
    """Views for managing tags"""
    queryset = Tag.objects.all()
    lookup_field = 'tag'
    serializer_class = TagSerialiser

class CategoryViewSet(viewsets.ModelViewSet):
    """Views for managing categories"""
    queryset = Category.objects.all()
    lookup_field = 'category'
    serializer_class = CategorySerialiser

class TypeViewSet(viewsets.ModelViewSet):
    """Views for managing cart types"""
    queryset = Type.objects.all()
    lookup_field = 'type'
    serializer_class = TypeSerialiser

class CartViewSet(viewsets.ModelViewSet):
    """Views for managing carts"""
    queryset = Cart.objects.all()
    lookup_field = 'label'
    serializer_class = CartSerialiser
