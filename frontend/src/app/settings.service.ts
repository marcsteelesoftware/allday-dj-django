import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Settings } from './settings'

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private settingsUrl = '/api/settings/';

  constructor(
    private http: HttpClient
  ) { }

  getSettings(): Observable<Settings> {
    return this.http.get<Settings>(this.settingsUrl);
  }

}
