'''
    Provides pagination services for AllDay DJ
'''

from rest_framework.pagination import PageNumberPagination

class AllDayDJPagination(PageNumberPagination):
    """Provides the default pagination configuration for AllDay DJ"""
    page_size = 0
    page_size_query_param = 'page_size'
