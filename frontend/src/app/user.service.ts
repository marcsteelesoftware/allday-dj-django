import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    private static readonly USER_URL = '/api/user/';
    private static readonly LOGIN_URL = '/api/user/login/';
    private static readonly LOGOUT_URL = '/api/user/logout/';

    constructor(private http: HttpClient) { }

    getUser(): Observable<User> {
        return this.http.get<User>(UserService.USER_URL);
    }

    /**
     * Attempts to log in a user.
     * @param username The username to try.
     * @param password The password to try.
     */

    login(username: string, password: string): Observable<User> {

        const params = {
            'username': username,
            'password': password
        };

        return this.http.post<User>(UserService.LOGIN_URL, params);

    }

    logout() {
        return this.http.get(UserService.LOGOUT_URL);
    }

}
