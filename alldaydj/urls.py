"""
AllDay DJ URLs
"""
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as tokenviews
from alldaydj import views

# pylint: disable=invalid-name
# Above line is because pylint doesn't like Django naming conventions

# API router

router = DefaultRouter()
router.register(r'tag', views.TagViewSet)
router.register(r'category', views.CategoryViewSet)
router.register(r'type', views.TypeViewSet)
router.register(r'cart', views.CartViewSet)

# API documentation

schema_view = get_schema_view(title='AllDay DJ API')

# URL patterns


urlpatterns = [
    path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    path('schema/', schema_view),
    path('api/', include(router.urls)),
    path('api/user/', views.current_user),
    path('api/user/token/', tokenviews.obtain_auth_token),
    path('api/user/login/', views.api_login, name='apilogin'),
    path('api/user/logout/', views.api_logout, name='apilogout'),
    path('api/settings/', views.settings, name='settings'),
    path('api/audio/<label>/wave/', views.wave_file),
    path('api/audio/<label>/compressed/', views.compressed_file),
    re_path(r'(.*)', views.index, name='index'),
]
