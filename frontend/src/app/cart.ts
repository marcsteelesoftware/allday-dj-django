export class Cart {
    label: string;
    title: string;
    artists: string[] = [];
    display_artist: string;
    year: number;
    isrc?: String;
    record_label?: string;
    composer_arranger?: string;
    publisher?: string;
    promoter?: string;
    sweeper: boolean;
    marker_audio_start: number;
    marker_audio_end: number;
    marker_intro_start: number;
    marker_intro_end: number;
    marker_segue: number;
    categories: string[] = [];
    tags: string[] = [];
}
