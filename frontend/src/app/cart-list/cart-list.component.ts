import { Component, OnInit, ViewChild } from '@angular/core';
import { Cart } from '../cart';
import { CartService } from '../cart.service';
import { isNullOrUndefined } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ComponentBase } from '../component-base';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent extends ComponentBase implements OnInit {

  carts: Cart[];
  pageSize = 10;
  currentPage = 1;
  totalResults = 0;
  previewCart: Cart;
  deleteSelectedCart: Cart;

  @ViewChild('confirmModal', {'static': true}) private confirmModal;
  @ViewChild('errorModal', {'static': true}) private errorModal;

  constructor(
    private cartService: CartService,
    protected modalService: NgbModal
  ) {
    super(modalService);
  }

  ngOnInit() {

    // Trigger the initial search

    this.search();

  }

  /**
   * Performs a search
   */

  search() {

    this.cartService.search(this.currentPage, this.pageSize).subscribe(
      (cartPage) => {

        // Pull in the results

        this.carts = cartPage.results;
        this.totalResults = cartPage.count;

      }
    );

  }

  /**
   * Deletes a selected cart.
   * @param cart The cart we wish to delete.
   * @param confirm Indicates if the user has confirmed yet.
   */

  deleteCart(cart: Cart, confirm = false) {

    // Sanity check

    if (cart === null || cart === undefined) {
      return;
    }

    if (confirm) {

      // We've been told to delete

      this.cartService.deleteCart(cart).subscribe(
        (deletedCart) => {

          // Success - refresh the search

          this.modalService.dismissAll();
          this.search();

        },
        (error) => {

          // Error - tell the user

          this.displayError(true, this.errorModal, `Failed to delete cart.`);


        }
      );

      // Unload the preview player (if needed)
      // Then bin off our selection for deletion

      if (
        !(this.previewCart === null || this.previewCart === undefined)
        && this.previewCart === cart) {
          this.previewCart = null;
        }

      this.deleteSelectedCart = null;

    } else {

      this.deleteSelectedCart = cart;
      this.modalService.open(this.confirmModal);

    }

  }

}
