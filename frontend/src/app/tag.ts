/**
 * Represents a scheduler tag.
 */

export class Tag {
    tag: string;
}