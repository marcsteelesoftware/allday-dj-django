import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Cart } from './cart';
import { Type } from './type';
import { Category } from './category';
import { Tag } from './tag';
import { Paginator } from './paginator';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cartUrl = '/api/cart/';
  private typeUrl = '/api/type/';
  private tagUrl = '/api/tag/';
  private categoryUrl = '/api/category/';
  private audioUrl = '/api/audio/';
  private httpOptions: any;

  constructor(private http: HttpClient, private cookieService: CookieService) {

    // Set the CSRF protection cookie

    let csrf = this.cookieService.get('csrftoken');

    if (typeof(csrf) === 'undefined') {
      csrf = '';
    }

    this.httpOptions = {
      headers: new HttpHeaders({
        'X-CSRFToken': csrf
      })
    };

  }

  /**
   * Fetches a cart from the API.
   * @param label The label of the cart we're interested in.
   */

  getCart(label: String): Observable<Cart> {
      return this.http.get<Cart>(`${this.cartUrl}${label}/`);
  }

  /**
   * Attempts to save a cart to the server.
   * @param cart The cart to save back to the API.
   * @param newCart Indicates if this is a new cart.
   */

  saveCart(cart: Cart, newCart: boolean): Observable<HttpEvent<Cart>> {
    if (newCart) {
      return this.http.post<Cart>(this.cartUrl, cart, this.httpOptions);
    } else {
      return this.http.put<Cart>(`${this.cartUrl}${cart.label}/`, cart, this.httpOptions);
    }
  }

  /**
   * Attempts to delete a cart
   * @param cart The cart we wish to delete.
   */

  deleteCart(cart: Cart): Observable<HttpEvent<Cart>> {
    return this.http.delete<Cart>(`${this.cartUrl}${cart.label}/`, this.httpOptions);
  }

  /**
   * Uploads audio to the server.
   * @param cart The cart to save the audio for.
   * @param audioBlob The audio we want to save.
   */

  saveAudio(cart: Cart, audioBlob: Blob) {

    // Generate the form (with the audio file in it)

    const formData = new FormData();
    formData.append('audio', audioBlob);

    // Perform the post

    return this.http.post(
      `${this.audioUrl}${cart.label}/wave/`,
      formData,
      {
        headers: this.httpOptions.headers,
        reportProgress: true,
        observe: 'events'
      }
    );
  }

  /**
   * Downloads the audio file for a specified cart.
   * @param cart The cart we want the audio for.
   * @param compressed TRUE if the audio is compressed.
   */

  getAudio(cart: Cart, compressed = true): Observable<Blob> {

    if (compressed) {
      return this.http.get(
        `${this.audioUrl}${cart.label}/compressed/`,
        {
          responseType: 'blob'
        }
      );
    } else {
      return this.http.get(
        `${this.audioUrl}${cart.label}/wave/`,
        {
          responseType: 'blob'
        }
      );
    }

  }

  /**
   * Fetches the list of possible types from the API.
   */

  getTypes(): Observable<Type[]> {
    return this.http.get<Type[]>(this.typeUrl);
  }

  /**
   * Fetches the list of possible categories from the API.
   */

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoryUrl);
  }

  /**
   * Fetches the list of possible tags from the API.
   */

  getTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.tagUrl);
  }

  /**
   * Searches for carts.
   * @param page The page we're currently on in the search.
   * @param pageSize The size of page we're working with.
   */

  search(page: number, pageSize: number): Observable<Paginator<Cart>> {

    const params = new HttpParams({
      fromObject: {
        'page': `${page}`,
        'page_size': `${pageSize}`
      }
    });

    return this.http.get<Paginator<Cart>>(
      this.cartUrl,
      {
        params: params
      }
    );

  }

}
