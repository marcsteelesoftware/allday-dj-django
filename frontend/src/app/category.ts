/**
 * Represents a category in the system.
 */

 export class Category {
     category: string;
 }