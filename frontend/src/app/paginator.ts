/**
 * Holds the paginated results from a query or search.
 */

export class Paginator<T> {

    count: number;
    results: T[];

}