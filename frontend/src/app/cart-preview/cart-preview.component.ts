import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Cart } from '../cart';
import { CartService } from '../cart.service';
import { AudioService } from '../audio.service';
import { interval, Subscription } from 'rxjs';

/**
 * Represents the possible player states.
 */

enum PlayerState {
  Unloaded,
  Loading,
  Loaded,
  Playing,
  Paused,
  Error
}

@Component({
  selector: 'app-cart-preview',
  templateUrl: './cart-preview.component.html',
  styleUrls: ['./cart-preview.component.css']
})
export class CartPreviewComponent implements OnInit {

  private _cart: Cart;
  private state: PlayerState;
  private context: AudioContext;
  private error: any;
  private source: AudioBufferSourceNode;
  private startTime: number;
  private position = 0;
  private length = 0;
  private intervalSubscription: Subscription;

  @Input()
  set cart(cart: Cart) {

    // Unload any existing audio

    // Assign and load the new cart

    this._cart = cart;
    this.load();

  }

  get cart() {
    return this._cart;
  }

  constructor(
    private cartService: CartService,
    private audioService: AudioService,
    private changeDetectorReference: ChangeDetectorRef
  ) {
    this.state = PlayerState.Unloaded;
  }

  ngOnInit() {
  }

  /**
   * Loads the audio into the player
   */

  private load() {

    // Sanity check

    if (this.state !== PlayerState.Unloaded || !this._cart) {
      return;
    }

    // Attempt to download the audio

    this.state = PlayerState.Loading;

    this.cartService.getAudio(this._cart).subscribe(
      (file) => {

        // Load the audio file we've downloaded

        this.audioService.loadFromBlob(file, this.audioContext)
          .then((audioBuffer) => {

            // Hook up the audio source and destination

            this.source = this.audioContext.createBufferSource();
            this.source.buffer = audioBuffer;
            this.source.connect(this.audioContext.destination);

            // Calculate the lengths

            this.length = this.cart.marker_audio_end - this.cart.marker_audio_start;

            // Confirm our state and start playing

            this.state = PlayerState.Loaded;
            this.play();

          })
          .catch((error) => {
            this.state = PlayerState.Error;
            this.error = error;
          });

      },
      (error) => {
        this.state = PlayerState.Error;
        this.error = error;
      }
    );

  }

  /**
   * Plays the loaded audio
   */

  private play(): void {

    // Sanity check

    if (this.state === PlayerState.Loaded) {

      // Start the audio

      this.startTime = this.context.currentTime;
      this.source.start(0, this.cart.marker_audio_start);

      // Start the position calculator

      this.intervalSubscription = interval(250).subscribe(
        () => this.calculateTimers()
      );

      // Confirm our state change

      this.state = PlayerState.Playing;

    }

  }

  /**
   * Calculates the current timers.
   */

  private calculateTimers(): void {

    if (this.state === PlayerState.Playing) {

      // We're playing, calculate and let the UI know

      this.position = this.context.currentTime - this.startTime;
      this.position = Math.min(this.position, this.length);
      this.changeDetectorReference.detectChanges();

    }

  }

  /**
   * Obtains the current cart position as a percentage.
   * @returns The current cart position.
   */

  get positionPercent(): number {

    if (this.state === PlayerState.Playing || this.state === PlayerState.Paused) {
      return Math.max(
        0,
        Math.min(
          this.position / this.length * 100,
          100
        )
      );
    } else {
      return 0;
    }

  }

  /**
   * Obtains the current cart positon in minutes.
   * @returns The current cart positon.
   */

  get positionMins(): number {

    if (this.state === PlayerState.Playing || this.state === PlayerState.Paused) {
      return Math.max(0, Math.floor(this.position / 60));
    } else {
      return 0;
    }

  }

  /**
   * Obtains the current cart position in seconds.
   * @returns The current cart position.
   */

  get positionSecs(): number {

    if (this.state === PlayerState.Playing || this.state === PlayerState.Paused) {
      return Math.max(0, this.position % 60);
    } else {
      return 0;
    }

  }

  /**
   * Obtains the length of the cart in minutes.
   * @returns The cart length.
   */

  get lengthMins(): number {

    if (this.length === null || this.length === undefined) {
      return 0;
    } else {
      return Math.max(0, Math.floor(this.length / 60));
    }

  }

  /**
   * Obtains the cart length in seconds.
   * @returns The cart length.
   */

  get lengthSecs(): number {

    if (this.length === null || this.length === undefined) {
      return 0;
    } else {
      return Math.max(0, this.length % 60);
    }

  }

  /**
   * Obtains the current audio context
   */

  private get audioContext(): AudioContext {

    if (!this.context) {
      this.context = new AudioContext();
    }

    return this.context;

  }

}

