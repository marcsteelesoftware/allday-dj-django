import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cart } from '../cart';
import { CartService } from '../cart.service';
import { SettingsService } from '../settings.service';
import * as WaveSurfer from 'wavesurfer.js';
import RegionPlugin from 'wavesurfer.js/dist/plugin/wavesurfer.regions.js';
import { HttpErrorResponse, HttpEventType, HttpEvent, HttpResponse } from '@angular/common/http';
import { Settings } from '../settings';
import { AudioService } from '../audio.service';
import { isNullOrUndefined } from 'util';
import { Type } from '../type';
import { Category } from '../category';
import { Tag } from '../tag';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { ComponentBase } from '../component-base';

/**
 * Component for adding and editing carts.
 */

@Component({
  selector: 'app-cart-editor',
  templateUrl: './cart-editor.component.html',
  styleUrls: ['./cart-editor.component.css']
})
export class CartEditorComponent extends ComponentBase implements OnInit {

  private static readonly markerNames = [
    'audioStart',
    'introStart',
    'introEnd',
    'segue',
    'audioEnd'
  ];

  private static readonly markerFields = [
    'marker_audio_start',
    'marker_intro_start',
    'marker_intro_end',
    'marker_segue',
    'marker_audio_end'
  ];

  cart: Cart;
  wavesurfer: WaveSurfer;
  context: AudioContext;
  loadingMessage: string;
  showCartEditorWaveform = false;
  settings: Settings;
  audio: AudioBuffer;
  audioEdited = false;
  audioBlob: Blob;
  newCart = false;
  allowAudioImport = true;
  types: Type[];
  categories: Category[];
  tags: Tag[];

  // tslint:disable-next-line:no-inferrable-types
  progressPercent: number = 0;

  @ViewChild('confirmModal', {static: true}) private confirmModal;
  @ViewChild('errorModal', {static: true}) private errorModal;
  @ViewChild('loadingModal', {static: true}) private loadingModal;

  constructor(
      private route: ActivatedRoute,
      private cartService: CartService,
      private settingsService: SettingsService,
      private audioService: AudioService,
      protected modalService: NgbModal,
      private router: Router
  ) {
    super(modalService);
  }

  ngOnInit() {

    // Setup the waveform editor

    this.wavesurfer = WaveSurfer.create({
        container: '#cart-edit-waveform',
        waveColor: 'violet',
        progressColor: 'purple',
        scrollParent: true,
        splitChannels: true,
        autoCenter: false,
        plugins: [
          RegionPlugin.create({

          })
        ]
    });

    // Load any settings we need

    this.settingsService.getSettings().subscribe(
      settings => this.settings = settings,
      error => this.handleGenericLoadError('settings', error)
    );

    this.cartService.getTypes().subscribe(
      types => this.types = types,
      error => this.handleGenericLoadError('types', error)
    );

    this.cartService.getCategories().subscribe(
      categories => this.categories = categories,
      error => this.handleGenericLoadError('categories', error)
    );

    this.cartService.getTags().subscribe(
      tags => this.tags = tags,
      error => this.handleGenericLoadError('tags', error)
    );

    // Load the cart

    this.getCart();

  }

  /**
   * Gets the cart from server or creates from new.
   */

  getCart(): void {

    // Show the loading UI

    this.displayLoading(true, 'Loading cart...');

    // Load from API or create a new cart object

    if (this.route.snapshot.paramMap.has('label')) {

      const label = this.route.snapshot.paramMap.get('label');
      this.cartService.getCart(label).subscribe(
          cart => {

            // We got a cart, go grab the audio

            this.cart = cart;
            this.getAudio();

          },
          error => this.handleHttpLoadError(error, this.errorModal)
      );

    } else {
        this.cart = new Cart();
        this.newCart = true;
    }

    this.displayLoading(false, null);

  }

  /**
   * Handles importing an audio file.
   * @param files A list of files opened by the user.
   */

  async handleImportFile(files: FileList) {

    // Sanity check

    if (files.length === 0) {
      return;
    } else if (files.length !== 1) {
      this.displayError(true, this.errorModal, `You selected ${files.length} file(s). We only need one!`);
      return;
    }

    // Stop any previous playing audio

    if (this.wavesurfer.isPlaying()) {
      this.wavesurfer.stop();
    }

    this.wavesurfer.clearRegions();

    // Update the user interface

    this.displayLoading(true, 'Loading audio file from disk...');
    this.showCartEditorWaveform = false;
    this.allowAudioImport = false;
    this.audioEdited = true;

    // Read the file from disk

    this.loadAudioContext();
    const audioFile = files.item(0);

    try {

      this.audio = await this.audioService.loadFromFile(audioFile, this.context);

    } catch (error) {
      this.displayError(true, this.errorModal, `Failed to load audio from ${audioFile.name}. Reason: ${error}`);
      this.allowAudioImport = true;
      return;
    }

    // Normalise the audio

    try {

      this.displayLoading(true, 'Normalising audio...');
      this.audio = await this.audioService.normalise(this.audio, this.settings);

    } catch (error) {
      this.displayError(true, this.errorModal, `Failed to normalise the audio from ${audioFile.name}. Reason: ${error}`);
      return;
    }

    // Set some sensible default for markers

    const audioLength = Math.round(10 * this.audio.getChannelData(0).length / this.audio.sampleRate) / 10;

    this.cart.marker_audio_start = 0.0;
    this.cart.marker_intro_start = 0.0;
    this.cart.marker_intro_end = 0.0;
    this.cart.marker_segue = audioLength;
    this.cart.marker_audio_end = audioLength;

    // Read the metadata

    try {

      this.displayLoading(true, 'Reading metadata...');
      this.cart = await this.audioService.readMetadata(audioFile, this.cart);

    } catch (error) {
      this.displayError(true, this.errorModal, `Failed to read the metadata from ${audioFile.name}. Reason: ${error}`);
      this.allowAudioImport = true;
      return;
    }

    // Generate the wave file

    try {

      this.displayLoading(true, 'Generating wave file...');
      this.audioBlob = await this.audioService.generateWaveFile(this.cart, this.audio);

    } catch (error) {
      this.displayError(true, this.errorModal, `Failed to generate the wave file. Reason: ${error}`);
      this.allowAudioImport = true;
      return;
    }

    // Show the UI

    this.displayWaveform();
    this.displayLoading(false, null);
    this.allowAudioImport = true;

  }

  /**
   * Downloads the audio for the specified cart.
   */

  private getAudio() {

    // Show the loading screen

    this.displayLoading(true, 'Downloading audio...');

    // Download the file

    this.cartService.getAudio(this.cart).subscribe(
      (audioBlob) => {

        // Clear the loading screen

        this.displayLoading(false, null);

        // Load the audio onscreen

        this.audioBlob = audioBlob;
        this.displayWaveform();

      },
      (error) => {
        this.handleHttpLoadError(error, this.errorModal);
      }
    );

  }

  /**
   * Displays the waveform onscreen.
   */

  private displayWaveform() {

    const cartEditor = this;

    this.wavesurfer.on('ready', function() {

      cartEditor.wavesurfer.addRegion({
        id: 'intro',
        start: cartEditor.cart.marker_intro_start,
        end: cartEditor.cart.marker_intro_end,
        drag: false,
        resize: true,
        color: '#42A94880'
      });

      cartEditor.wavesurfer.addRegion({
        id: 'extro',
        start: cartEditor.cart.marker_segue,
        end: cartEditor.cart.marker_audio_end,
        drag: false,
        resize: true,
        color: '#FFDC0080'
      });

      cartEditor.wavesurfer.addRegion({
        id: 'start',
        start: 0,
        end: cartEditor.cart.marker_audio_start,
        drag: false,
        resize: true,
        color: '#A9444280'
      });

      cartEditor.wavesurfer.addRegion({
        id: 'end',
        start: cartEditor.cart.marker_audio_end,
        end: cartEditor.wavesurfer.getDuration(),
        drag: false,
        resize: true,
        color: '#A9444280'
      });

    });

    this.wavesurfer.on('region-update-end', function(region) {

      // Figure out what marker and position we're adjusting

      let marker: string;
      let position = -1;

      if (region.id === 'intro') {

        if (region.start !== cartEditor.cart.marker_intro_start) {
          marker = 'introStart';
          position = region.start;
        } else if (region.end !== cartEditor.cart.marker_intro_end) {
          marker = 'introEnd';
          position = region.end;
        }

      } else if (region.id === 'extro') {

        if (region.start !== cartEditor.cart.marker_intro_start) {
          marker = 'segue';
          position = region.start;
        } else if (region.end !== cartEditor.cart.marker_intro_end) {
          marker = 'audioEnd';
          position = region.end;
        }

      } else if (region.id === 'start') {

        // Override the user here, they can only adjust the end

        if (region.start !== cartEditor.cart.marker_intro_start) {
          marker = 'audioStart';
          position = region.end;
        } else if (region.end !== cartEditor.cart.marker_intro_end) {
          marker = 'audioStart';
          position = region.end;
        }

      } else if (region.id === 'end') {

        // Similar but only the start this time

        if (region.start !== cartEditor.cart.marker_intro_start) {
          marker = 'audioEnd';
          position = region.start;
        } else if (region.end !== cartEditor.cart.marker_intro_end) {
          marker = 'audioEnd';
          position = region.start;
        }

      }

      // Make the adjustment

      if (position >= 0) {
        cartEditor.adjustMarker(marker, position);
        cartEditor.updateWavesurferRegions();
      }

    });

    this.wavesurfer.loadBlob(this.audioBlob);
    this.showCartEditorWaveform = true;
    this.displayLoading(false, null);

  }

  /**
   * Updates the wavesurfer regions.
   */

  private updateWavesurferRegions() {

    this.wavesurfer.regions.clear();

    this.wavesurfer.addRegion({
      id: 'intro',
      start: this.cart.marker_intro_start,
      end: this.cart.marker_intro_end,
      drag: false,
      resize: true,
      color: '#42A94880'
    });

    this.wavesurfer.addRegion({
      id: 'extro',
      start: this.cart.marker_segue,
      end: this.cart.marker_audio_end,
      drag: false,
      resize: true,
      color: '#FFDC0080'
    });

    this.wavesurfer.addRegion({
      id: 'start',
      start: 0,
      end: this.cart.marker_audio_start,
      drag: false,
      resize: true,
      color: '#A9444280'
    });

    this.wavesurfer.addRegion({
      id: 'end',
      start: this.cart.marker_audio_end,
      end: this.wavesurfer.getDuration(),
      drag: false,
      resize: true,
      color: '#A9444280'
    });

  }
  /**
   * Handles clicking the cue button on the screen.
   * @param marker The name of the marker we're interested in.
   */

  public handleCueClick(marker: String) {

    // Sanity check

    if (!(this.displayWaveform && this.wavesurfer.isReady)) {
      console.log('Ignoring cue button click as we\'re not ready for it.');
      return;
    }

    // Figure out the marker we're at and jump to the audio point

    let seekPosition = 0;

    if (marker === 'audioStart') {
      seekPosition = this.cart.marker_audio_start;
    } else if (marker === 'introStart') {
      seekPosition = this.cart.marker_intro_start;
    } else if (marker === 'introEnd') {
      seekPosition = this.cart.marker_intro_end;
    } else if (marker === 'segue') {
      seekPosition = this.cart.marker_segue;
    } else if (marker === 'audioEnd') {
      seekPosition = this.cart.marker_audio_end;
    } else {
      console.log(`Got a cue click from an unexpected source: ${marker}.`);
      return;
    }

    this.wavesurfer.seekAndCenter(Math.min(seekPosition / this.wavesurfer.getDuration(), 1.0));

    // Don't trigger a play audio if we've seeked to (or post) the end

    if (seekPosition >= this.wavesurfer.getDuration()) {
      return;
    }

    // Play if we're not already

    if (!this.wavesurfer.isPlaying()) {
      this.wavesurfer.play();
    }

  }

  /**
   * Handles clicking the set marker buttons.
   * @param marker The marker we want to set.
   */

  public handleSetClick(marker: string) {

    // Sanity check

    if (!(this.displayWaveform && this.wavesurfer.isReady)) {
      console.log('Ignoring set button click as we\'re not ready for it.');
      return;
    }

    // Read in the position and adjust our marker

    const position = this.wavesurfer.getCurrentTime();
    this.adjustMarker(marker, position);

    // Now refresh our regions

    this.updateWavesurferRegions();

  }

  /**
   * Adjusts a marker based on name.
   * @param marker The marker we want to adjust.
   * @param position The position we'll be adjusting to.
   */

  private adjustMarker(marker: string, position: number) {

    // Find the marker we're interested in

    const index = CartEditorComponent.markerNames.indexOf(marker);
    if (index === -1) {
      console.log(`Ingoring set button click as ${marker} is not in our list of valid markers.`);
      return;
    }

    // Round the marker to appropriate precision

    position = Math.round(position * 10) / 10;

    // Adjust our marker(s)

    for (const currentMarkerIndex of Object.keys(CartEditorComponent.markerFields)) {

      const i = parseInt(currentMarkerIndex, 10);

      if (i < index) {

        if (this.cart[CartEditorComponent.markerFields[i]] > position) {
          this.cart[CartEditorComponent.markerFields[i]] = position;
        }

      } else if (i === index) {

        this.cart[CartEditorComponent.markerFields[i]] = position;

      } else {

        if (this.cart[CartEditorComponent.markerFields[i]] < position) {
          this.cart[CartEditorComponent.markerFields[i]] = position;
        }

      }


    }

  }

  /**
   * Handles a marker field being manually updated by the user.
   * @param marker The marker we're adjusting.
   */

  private handleMarkerChange(marker: string) {

    // Sanity check

    if (!(this.displayWaveform && this.wavesurfer.isReady)) {
      console.log('Ignoring marker manual adjustment as we\'re not ready for it.');
      return;
    }

    // Retrieve the value and sanity check it

    const markerIndex = CartEditorComponent.markerNames.indexOf(marker);
    if (markerIndex < 0) {
      console.log(`Ignoring marker manual adjustment as ${marker} is not valid.`);
      return;
    }

    let position = this.cart[CartEditorComponent.markerFields[markerIndex]];
    position = Math.max(position, 0.0);
    position = Math.min(position, this.wavesurfer.getDuration());

    // Make the adjustment and update the UI

    this.adjustMarker(marker, position);
    this.updateWavesurferRegions();

  }

  /**
   * Loads the audio context.
   */

  private loadAudioContext() {

    // Sanity check

    if (!this.context) {
        this.context = new AudioContext();
    }

    return this.context;

  }

  /**
   * Displays a loading message.
   * @param loading Whether to display the message or not.
   * @param message The message.
   */

  private displayLoading(loading: boolean, message: string) {

    this.loadingMessage = message;
    this.modalService.dismissAll();

    if (loading) {
      this.modalService.open(this.loadingModal);
    }

  }

  /**
   * Adds an artist to the current cart.
   */

  public addArtist() {

    // Sanity check

    if (this.cart.artists === null || this.cart.artists === undefined) {
       this.cart.artists = [];
    }

    // Perform the add

    this.cart.artists.push('');

  }

  /**
   * Deletes a specified artist from the cart.
   * @param artist The artist to delete.
   */

  public deleteArtist(artist: string) {

    // Sanity check

    if (this.cart.artists === null || this.cart.artists === undefined) {
      return;
    }

    // Perform the delete

    const artistIndex = this.cart.artists.indexOf(artist);
    if (artistIndex >= 0) {
      this.cart.artists.splice(artistIndex, 1);
    }

  }

  /**
   * Tracks artists on the UI.
   * @param index The index of the artist.
   * @param item  The artist value.
   */

  public trackArtist(index: number, item: string) {
    return index;
  }

  /**
   * Returns the style for a type.
   */

  getTypeStyle(type: Type) {

    const style = {
      'background-color': type.colour,
      'color': '#000000'
    };

    // Sanity check, defaulting to back

    if (!type.colour) {
        return style;
    }

    const regex = new RegExp('#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})');
    const matches = regex.exec(type.colour);
    if (!matches || matches.length < 4) {
        return style;
    }

    // Parse the colour values

    const red = parseInt(matches[1], 16);
    const green = parseInt(matches[2], 16);
    const blue = parseInt(matches[3], 16);

    // Now work out, black or white

    const brightness = red * 0.299 + green * 0.587 + blue * 0.114;
    if (brightness < 186) {
        style['color'] = '#FFFFFF';
    }

    return style;

  }

  /**
   * Obtains the maximum year the user can enter into the form.
   */

  getMaxYear() {

    const now = new Date();
    return this.settings ? now.getFullYear() + this.settings.years_future : now.getFullYear();

  }

  /**
   * Attempts to save the cart to the backend.
   */

  saveCart(overwrite: boolean, calledLocally = false) {

    // Make sure there's some audio

    if (!this.audioBlob) {
      this.displayError(true, 'You must load some audio in order to save this cart!', this.errorModal);
      return;
    }

    // Sanity check if we're a new cart, require overwrite confirmation if so

    if (this.newCart && !overwrite && !calledLocally) {

      this.cartService.getCart(this.cart.label).subscribe(
        (cart) => {
          this.modalService.open(this.confirmModal).result.then(
            (result) => {
              this.saveCart(true);
            }
          );
        },
        (error) => {
          if (error.status === 404) {
            this.saveCart(false, true);
          } else {
            this.handleHttpLoadError(error, this.errorModal);
          }
        }
      );

      return;

    }

    // Save the cart

    this.displayLoading(true, 'Saving cart to database...');
    this.cartService.saveCart(this.cart, this.newCart && !overwrite).subscribe(
      (event) => {

        // Make sure we only crack on if there's an audio change

        if (!this.audioEdited) {
          this.modalService.dismissAll();
          this.newCart = false;
          return;
        }

        // Generate the wave file

        this.displayLoading(true, 'Generating audio file...');
        const cartEditor = this;

        this.audioService.generateWaveFile(this.cart, this.audio).then(
          function(value) {

            // Update the blob

            cartEditor.audioBlob = value;

            // Post it to the server

            cartEditor.displayLoading(true, 'Uploading audio file...');
            cartEditor.cartService.saveAudio(cartEditor.cart, cartEditor.audioBlob).subscribe(
              (uploadEvent) => {

                if ('type' in uploadEvent) {

                  const httpEvent = <HttpEvent<any>>uploadEvent;

                  if (httpEvent.type === HttpEventType.UploadProgress) {
                    cartEditor.progressPercent = Math.round(100 * httpEvent.loaded / httpEvent.total);
                  }

                  if (httpEvent.type === HttpEventType.Response) {

                    if (httpEvent.status === 200) {

                      // Upload complete, cleanup

                      cartEditor.progressPercent = -1;
                      cartEditor.audioEdited = false;
                      cartEditor.modalService.dismissAll();

                      // Redirect if we're a new cart

                      if (cartEditor.newCart) {
                        cartEditor.router.navigate([`/cart/edit/${cartEditor.cart.label}`]);
                        cartEditor.newCart = false;
                      }

                    } else {
                      cartEditor.displayError(
                        true,
                        this.errorModal,
                        `Ran into a problem uploading the audio. Specifically: ${httpEvent.body}.`
                      );
                      cartEditor.progressPercent = -1;
                    }
                  }

               }

              },
              (error) => {

                console.log(error);
                cartEditor.progressPercent = -1;
                cartEditor.handleHttpLoadError(error, this.errorModal);

              });

          }
        );

      }
    );

  }

  /**
   * Handles errors with the settings loading process.
   * @param source The source of the error.
   * @param error The error to be handled.
   */

  private handleGenericLoadError(source: string, error: HttpErrorResponse) {

    if (error.error instanceof ErrorEvent) {
      console.log(`A local error occurred loading the ${source}: ${error.error.message}`);
    } else {
      console.log(`A server error occurred loading the ${source}. Server status code ${error.status} with message ${error.error.message}`);
    }

    // Fall back to using defaults

    this.settings = new Settings();

  }

}
